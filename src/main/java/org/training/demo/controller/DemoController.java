package org.training.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	@GetMapping(value = "/message")
	public String getMessage() {
		return "This is Docker simple project";
	}
}
